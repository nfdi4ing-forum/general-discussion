__[TOC]__


# General Discussion

![](/img/cc-logo.png)

General Topics around or relevant for NFDI4Ing  
[Browse Threads or create a new one](https://git.rwth-aachen.de/nfdi4ing-forum/general-discussion/-/issues)



# How-To 
At this point a guide on how to use GitLab as forum will be created.  
For information on all of GitLab's capabilities refer to the excellent [help sites](https://git.rwth-aachen.de/help) and [documentation](https://docs.gitlab.com)  

### Structure

A usual forum ( or bulletin board) consists of Subforums, Threads, and posts.
This is a hierarchical system that we can rebuild in Gitlab with Groups, Projects, Issues and comments. 
But Gitlab offers further possibilities to sort and organize outside of this hierarchy - labels can be used to tag threads, and epics and milestones can serve as further organizational structures.
We would like to keep it simple by only using Labels, Projects, Threads and comments.

### Create a new thread (issue)
A new ~thread~ (here: issue) is created for a project, you can however view the collected issues for a group covering multiple projects.  
The third entry in the left sidebar and is about issues, choose list and find the blue button 'new issue' on the top right side and you're good to go.  
A title is mandatory, but the description and useful labels help a lot, and if you know someone responsible, you can also @ or assign that person. Just dont spam people.  
<img src="/img/Issue-List-Board.png"  height="180" />  


### References and filters
Gitlab offers advanced referencing functionality, specific characters will stand for specific units in the Gitlab system and Gitlab will try to autocomplete those for you.
This makes it easy to reference an issue with a `#` or a user with `@`.
The same units can be used as filters, such as `assignee != @user123` to not show any issues assigned to user123,

You can find a table of autocomplete-characters [here](https://git.rwth-aachen.de/help/user/project/autocomplete_characters.md#autocomplete-characters)
Additionally you can link lines (5-8) of a file (named 'file.ext') by adding them with `file.ext#5-8`.


#### Linking
You can easily link files (even specific lines), other projects/groups or Wiki articles. Gitlab will recognize relative links.  
The syntax for a link is `[Shown text](link)`.  
 [This](https://git.rwth-aachen.de/nfdi4ing-forum/general-discussion/-/issues/1) 
 [and this](general-discussion#1)  as well as (#1) will link to the same issue written in an issue comment.
 ```
 [This](https://git.rwth-aachen.de/nfdi4ing-forum/general-discussion/-/issues/1)  
 [and this](general-discussion#1)  as well as (#1) will link to the same issue written in an issue comment.
 ```

#### Listing and management
The threads (issues in gitlab) can be shown as __list__ or in the __issue boards__ (development boards)

<img src="/img/Issue-List-Board.png"  height="360" />  

The issue boards allow you to filter for labels, show several lists next to each other and keep fixxed filters per board.  
<details><summary>Click to view screenshots</summary>
<img src="/img/Issue-Board-open-close.png"  height="240" />  
<img src="/img/Issue-Board-Filter.png"  height="360" />  

</details>

